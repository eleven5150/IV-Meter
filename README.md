**Задача данного проекта - разработка стенда для лабораторных работ по микроэлектронике**

Данное устройство - стенд для снятие ВАХ полупроводниковых элементов - транзисторов, диодов, светодиодов, стабилитронов и тд.

Принцип работы основан на том, что с помощью ШИМ, который генерирует МК, мы управляем напряжением, подающимся на полупроводниковый элемент. В устройстве есть модуль, который снимает значение тока и напряжения на элементе. Таким образом по точкам мы можем выстроить ВАХ элемента. Значение тока и напряжения выводятся на дисплее. Сами полупроводниковые элементы находяться на отдельных платах для каждой лабораторной работы.

Для данного проекта разработана топология и 3D модель в Altium Designer

Ссылка на проект Altium: [Link](https://drive.google.com/file/d/1CPJsp4AX4DmG2ibffHCtya2CJA0st5MC/view?usp=sharing)

![Pic1](Pictures/Trace.jpg)
![Pic1](Pictures/3d.jpg)

Печатные платы были заказаны на JLCPCB

![Pic1](Pictures/pcb.jpg)
