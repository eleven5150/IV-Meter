/*
 * switch.h
 *
 *  Created on: Sep 1, 2021
 *      Author: Eleven
 */

#ifndef INC_SWITCH_H_
#define INC_SWITCH_H_

#include "main.h"
uint8_t switchfunc (uint8_t ccr, uint8_t dia, int8_t dif);

#endif /* INC_SWITCH_H_ */
