/*
 * SH1106.c
 *
 *  Created on: Nov 27, 2020
 *      Author: TDM
 */

#include "sh1106.h"
#include "main.h"

static uint8_t SH1106_Buffer[SH1106_WIDTH * SH1106_HEIGHT / 8]; // Screen buffer
static SH1106_t SH1106; // Screen object

//  Send a byte to the command register

static void sh1106_WriteCommand(uint8_t command)
{
	HAL_I2C_Mem_Write(&SH1106_I2C_PORT, SH1106_I2C_ADDR,0x00,1,&command,1,10);
	//HAL_I2C_Mem_Write_IT(&SH1106_I2C_PORT, SH1106_I2C_ADDR, 0x00, 1, &command, 1);
}

//
//	Initialize the oled screen
//
uint8_t sh1106_Init(void)
{
	// Wait for the screen to boot
	HAL_Delay(1000);

	/* Init LCD */
	sh1106_WriteCommand(SH1106_CMD_DISP_OFF); 	// Display OFF
	sh1106_WriteCommand(SH1106_CMD_SETMUX); 	// Set multiplex ratio (N, number of lines active on display)
	sh1106_WriteCommand(0x3F);					// 64 lines
	sh1106_WriteCommand(SH1106_CMD_SETOFFS);	// Set display offset
	sh1106_WriteCommand(0x00);
	sh1106_WriteCommand(SH1106_CMD_STARTLINE);	// Set display start line
	sh1106_WriteCommand(0x00);
	sh1106_WriteCommand(SH1106_CMD_SEG_INV);    // Column 127 is mapped to SEG0 (X coordinate inverted)
	sh1106_WriteCommand(SH1106_CMD_COM_INV);	// Scan from COM[N-1] to COM0 (N - mux ratio, Y coordinate inverted)
	sh1106_WriteCommand(SH1106_CMD_COM_HW);		// Set COM pins hardware configuration
	sh1106_WriteCommand(0x12);
	sh1106_WriteCommand(SH1106_CMD_CHARGE);		// Dis-charge / Pre-charge Period
	sh1106_WriteCommand(0x00);
	sh1106_WriteCommand(SH1106_CMD_CONTRAST);	// Contrast control
	sh1106_WriteCommand(0xFF);             		// Contrast: 255 - max level
	sh1106_WriteCommand(0x32);					// Set Pump voltage value 32H - 8.0(Power on)
	sh1106_WriteCommand(SH1106_CMD_EDOFF); 		// Display follows RAM content
	sh1106_WriteCommand(SH1106_CMD_INV_OFF);  	// Normal display mode
	sh1106_WriteCommand(SH1106_CMD_CLOCKDIV);	// Set display clock divide ratio/oscillator frequency
	sh1106_WriteCommand(0xF0);

	sh1106_WriteCommand(SH1106_CMD_DISP_ON);	// Display ON

	// Clear screen
	sh1106_Fill(Black);

	// Flush buffer to screen
	sh1106_UpdateScreen();

	// Set default values for screen object
	SH1106.CurrentX = 0;
	SH1106.CurrentY = 0;

	SH1106.Initialized = 1;

	return 1;
}

//
//  Fill the whole screen with the given color
//
void sh1106_Fill(SH1106_COLOR color)
{
	/* Set memory */
	uint32_t i;

	for(i = 0; i < sizeof(SH1106_Buffer); i++)
	{
		SH1106_Buffer[i] = (color == Black) ? 0x00 : 0xFF;
	}
}

//
//  Write the screenbuffer with changed to the screen
//
void sh1106_UpdateScreen(void)
{
	uint8_t i;

	for (i = 0; i < 8; i++) {
		sh1106_WriteCommand(0xB0 + i);
		sh1106_WriteCommand(0x00);
		sh1106_WriteCommand(0x10);

		HAL_I2C_Mem_Write(&SH1106_I2C_PORT, SH1106_I2C_ADDR, 0x40, 1, &SH1106_Buffer[SH1106_WIDTH * i], SH1106_WIDTH, 100);
		//HAL_I2C_Mem_Write_IT(&SH1106_I2C_PORT, SH1106_I2C_ADDR, 0x40, 1, &SH1106_Buffer[SH1106_WIDTH * i], SH1106_WIDTH);
	}
}

//
//	Draw one pixel in the screenbuffer
//	X => X Coordinate
//	Y => Y Coordinate
//	color => Pixel color
//
void sh1106_DrawPixel(uint8_t x, uint8_t y, SH1106_COLOR color)
{
	if (x >= SH1106_WIDTH || y >= SH1106_HEIGHT)
	{
		return; // Don't write outside the buffer
	}

	// Check if pixel should be inverted
	if (SH1106.Inverted)
	{
		color = (SH1106_COLOR)!color;
	}

	// Draw in the right color
	if (color == White)
	{
		SH1106_Buffer[x + (y / 8) * SH1106_WIDTH] |= 1 << (y % 8);
	}
	else
	{
		SH1106_Buffer[x + (y / 8) * SH1106_WIDTH] &= ~(1 << (y % 8));
	}
}

//
//  Draw 1 char to the screen buffer
//	ch 		=> char om weg te schrijven
//	Font 	=> Font waarmee we gaan schrijven
//	color 	=> Black or White
//
char sh1106_WriteChar(char ch, FontDef Font, SH1106_COLOR color)
{
	uint32_t i, b, j;

	// Check remaining space on current line
	if (SH1106_WIDTH <= (SH1106.CurrentX + Font.FontWidth) || SH1106_HEIGHT <= (SH1106.CurrentY + Font.FontHeight))
	{
		return 0; // Not enough space on current line
	}

	// Use the font to write
	for (i = 0; i < Font.FontHeight; i++)
	{
		b = Font.data[(ch - 32) * Font.FontHeight + i];
		for (j = 0; j < Font.FontWidth; j++)
		{
			if ((b << j) & 0x8000)
			{
				sh1106_DrawPixel(SH1106.CurrentX + j, (SH1106.CurrentY + i), (SH1106_COLOR) color);
			}
			else
			{
				sh1106_DrawPixel(SH1106.CurrentX + j, (SH1106.CurrentY + i), (SH1106_COLOR)!color);
			}
		}
	}
	SH1106.CurrentX += Font.FontWidth;	// The current space is now taken
	return ch;	// Return written char for validation
}

//
//  Write full string to screenbuffer
//
char sh1106_WriteString(char* str, FontDef Font, SH1106_COLOR color)
{
	// Write until null-byte
	while (*str)
	{
		if (sh1106_WriteChar(*str, Font, color) != *str)
		{
			// Char could not be written
			return *str;
		}

		// Next char
		str++;
	}

	// Everything ok
	return *str;
}

//
//	Position the cursor
//
void sh1106_SetCursor(uint8_t x, uint8_t y)
{
	SH1106.CurrentX = x + 2;
	SH1106.CurrentY = y;
}



