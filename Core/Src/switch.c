/*
 * switch.c
 *
 *  Created on: Sep 1, 2021
 *      Author: Eleven
 */

#include "switch.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "main.h"

extern TIM_HandleTypeDef htim2;
extern uint8_t a1;

void switchdia (uint8_t na1, uint8_t ud){ // 0 - ; 1 +
	if (ud==0){
		switch(na1){
		case 2:
			HAL_GPIO_WritePin(VT3_GPIO_Port, VT3_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(VT4_GPIO_Port, VT4_Pin, GPIO_PIN_RESET);
			break;
		case 1:
			HAL_GPIO_WritePin(VT2_GPIO_Port, VT2_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(VT3_GPIO_Port, VT3_Pin, GPIO_PIN_RESET);
			break;
		case 0:
			HAL_GPIO_WritePin(VT_GPIO_Port, VT_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(VT2_GPIO_Port, VT2_Pin, GPIO_PIN_RESET);
			break;
		}
	} else {
		switch(na1){
				case 3:
					HAL_GPIO_WritePin(VT3_GPIO_Port, VT3_Pin, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(VT4_GPIO_Port, VT4_Pin, GPIO_PIN_SET);
					break;
				case 2:
					HAL_GPIO_WritePin(VT2_GPIO_Port, VT2_Pin, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(VT3_GPIO_Port, VT3_Pin, GPIO_PIN_SET);
					break;
				case 1:
					HAL_GPIO_WritePin(VT_GPIO_Port, VT_Pin, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(VT2_GPIO_Port, VT2_Pin, GPIO_PIN_SET);
					break;
				}
	}
}

uint8_t switchfunc (uint8_t ccr, uint8_t dia, int8_t dif){
	uint8_t nCCR=ccr;
	int8_t checkDif;
	checkDif=dif-ccr;

	if(checkDif>0){ //+
		if (dif>=100 && a1<3){ //change dia +
			a1+=1;
			switchdia(a1, 1);
			nCCR = dif - 100 + 1;
		} else if (dif<100) {
			nCCR = dif;
		}
	}
	else //-
	{
		if (dif<=0 && a1>0){ //change dia +
					a1-=1;
					switchdia(a1, 0);
					nCCR = dif + 100 - 1;
				} else if (dif>0) {
					nCCR = dif;
				}
		}
	return nCCR;
}

